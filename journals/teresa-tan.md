## Sept 15, 2022
Today I worked on:
* getting text to display white on project list and detail pages
* getting languages and percentage breakdown to display on barchart, no luck
* gradient buttons
* Getting Started section of README
* Updating Functionality section of README
* Implemented markdown styling to README

## Sept 14, 2022
Today I worked on:
* getting footer to stay fixed on bottom of page and get rid of the small gap between footer and bottom of page. Apparently css overrides bootstrap or vice versa??
* navbar styling
* header styling

## Sept 13, 2022
Today I worked on:
* css styling for main page

## Sept 12, 2022
Today I worked on:
* getting footer to stay fixed on bottom of page
* creating a react context hook to toggle dark mode

## Sept 9, 2022
Today I worked on:
* footer for main page
* debugging pollers

## Sept 8, 2022
Today I worked on:
* debugging accounts - logging into django admin page
* debugging pollers - unresolved still

## Sept 7, 2022
Today I worked on:
* pulling down jyoo and yehsun’s code and rectifying changes

## Sept 6, 2022
Today I worked on:
* create, delete, update user functions, however yehsun has not finalized authentication yet so could not proceed further
* worked with Liying and Brendan with pulling the README file from projects

## Sept 2, 2022
Today I worked on:
* finished polling from accounts to projects!

## Sept 1, 2022
Today I worked on:
* got together with Liying to work on polling data from projects to accounts and vice versa
* finished polling from projects to accounts
* created a get_users function to view all users

## August 31, 2022
Today I worked on:
* reading up on front-end authentication on Learn

## August 30, 2022
Today I worked on:
* found all remaining group ids for May, April, and CT cohorts, found 3 groups in our cohort that did not create
  a gitlab group which changes how our database is populated and how all functions will work
* decided as a group to pivot to searching by ProjectId
* found all ProjectIds for May, April, CT, and February cohorts with Liying’s help

## August 29, 2022
Today I worked on:
* refactoring existing get_tags function to work with LiYing’s helper functions
* created get_tech_stack function with endpoint working with Jyoo’s help
* took group consensus that we do not need the ability for user to create/delete tags

## August 26, 2022
Today I worked on:
* researched gitlab doc library to find how i can extract tech stacks used
* created project id model
* created project id function and endpoint working
* created project tag function and endpoint working
    - still need to create the create tag function
* got stuck on a bug when trying to extract tech stacks over the weekend and couldn’t get past it :disappointed:

## August 25, 2022
Today I worked on:
* created group_id model with brendan, yehsun, and liying with working view function and endpoint
* created encoders for group_id
* found some group ids previous and existing cohorts on gitlab to populate my database with
* created group and project avatars to render later when function works
* created a get_project_tags function and endpoint working

## August 24, 2022
Today I worked on:
* researching which gitlab stats we want to showcase
* set up application for CI/CD as a team
* deployed successfully to heroku as a team

## August 23, 2022
Today I worked on:
* reading gitlab library documentation

## August 22, 2022
Today I worked on:
* - divvied up duties: Jyoo + Brendan = users
* - Yehsun + Liying + me = projects
    - did pair programming with me as driver,Yehsun and Liying as navigators
    - created a few models and worked slowly thru the foreign key relationships
    - registered models, and installed project app in settings, however did not migrate yet as some of our models need foreign keys to Jyoo and Brendan’s User models but we regrouped at the end to discuss
    - we expect to finish the models by tomorrow and begin working on the view functions
    
## August 19, 2022
Today I worked on:
* Pulled copy of the project, created both volumes, ran docker containers, checked to make sure
everything was running correctly
* Did some research on how to implement a carousel react component for home page and sent link to group