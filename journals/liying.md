## September 16, 2022

Today, I worked on:

* putting everything together, doing some cleaning on the code, fix merging issues. writing md files...

## September 15, 2022

Today, I worked on:

* writing some test cases, doing some cleaning on the code, fix merging issues. writing md files for Api and data-models.

## September 14, 2022

Today, I worked on:

* creating get_user endpoint, and user profile page. Used redux to fetch backend data, still having some issues with react component.

## September 13, 2022

Today, I worked on:

* finishing the listing view for comments and detail view, successfully created component for listing comments and create new comment, having some trouble with creating a new comment, since the request.user always being anonymous.

## September 12, 2022

Today, I worked on:

* creating detail view for comment, starting working on react component for listing all the comments...

## September 9, 2022

Today, I worked on:

* adding more stuff in the list comments views and pulling from the main, fixing bugs...

## September 8, 2022

Today, I worked on:

* creating view functions for list/create comments. Having some issue with the user functions, gonna sync with everyone tomorrow and finish the rest.

## September 7, 2022

Today, I worked on:

* syncing with everyone, getting on the same page, switched database to postgres and changed dockerfile.dev.

## September 6, 2022

Today, I worked on:

* helping put up the detail page of every project, some charts still not showing nicely, need to figure out a way to solve that. Will look at the comments part and figure out how to implement that later this week.

## September 2, 2022

Today, I worked on:

* continue setting up the poller in the other microservice. integrating with other team members' code. sync with everyone and plan out what is next.

## September 1, 2022

Today, I worked on:

* finishing up the PieChart component, paired with Teresa working on the pollers between microservice. We ended up using django-crontab to set up the cronjob. Spending over 1.5 hr debugging with Daniel and finally figured out and successfully had one poller up and running.

## August 31, 2022

Today, I worked on:

* writing the react component for PieChart, using hooks like useState, useRef, useEffect, useParams. Also refactor the view function a little to display the data the way we want.

## August 30, 2022

Today, I worked on:

* helping find project_ids instead of group_ids, done some refactor job on views. Also did some readings on react component.

## August 29, 2022

Today, I worked on:

* writing some helper functions to make our API calls more efficient. I finished working on the commits view function by returning the json format we want, with the total commits numbers and also commits per person. Tomorrow might be working on creating REACT pie chart to display those data using a library called "recharts". Need to put that in the requirements.txt file and probably rebuild the docker containers? 

## August 25, 2022

Today, I worked on:

* yesterday's blocker got resolved. we divided into groups and played around with the API library. We collaboratively created view functions for get_group_members etc. At last, we assigned tasks. I will do the commits functions and explore more interesting metrics to display on our site.

## August 24, 2022

Today, I worked on:

* we together put efforts in creating CI/CD process in GitLab and have our website deployed on Heroku. It was a lot of trials and errors but we eventually got the test, build, deploy up and running. There was some issue in the react front page not displaying correctly. We decided to ask an instructor to get some clarifications.

## August 23, 2022

Today, I worked on:

* our team had a talk with Daniel and decided to go another direction with our project. So I looked into the GitLab docs and gathering relevant APIs we can adopt in our project.

## August 22, 2022

Today, I worked on:

* teaming with Teresa and Yehsun, acting as a navigator on "pair programming". We have most of the "project" model configured, leaving the value objects from "users" not implemented yet. Also, the five of us came up with Authentication initial seu-up together and pulled from main. Tomorrow, we will continue pair programming and working on the view functions and urls.

## August 19, 2022

Today, I worked on:

* helping in configuring the docker compose file. I started my own branch and was going to look in some GitLab API stuff during the weekend.
