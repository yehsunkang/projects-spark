import { NavLink } from "react-router-dom";
import Button from "react-bootstrap/Button";
import "./index.css";
import logo from "./rocket5.png";


export default function Nav(props) {

  return (
    <nav className="navbar navbar-expand-lg  ">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <NavLink className="navbar-brand nav-link" to="/"> <img src={logo} alt="logo" className="logo" width="30" />projectSpark </NavLink>
          <NavLink className="navbar-brand  nav-link  text-white" aria-current="page" to="/projects"> Search Projects</NavLink>
        </div>
        
        <div className="navbar" id="navbarCollapse">
          {!!props.userId ?
            <>

              <li className="nav">
                <NavLink className="btn rounded btn-outline-light me-2" to="/profiles/user" state={{ userId: props.userId }}>
                  Profile
                </NavLink>
              </li>
              <li className="nav">
                <NavLink className="btn rounded btn-outline-light me-2" to="/user/logout">
                  Log Out
                </NavLink>
              </li>

            </>
            :
            <>
              <li className="nav">
                <NavLink className="btn rounded btn-outline-light me-2" to="/user/login">
                  Log In
                </NavLink>
              </li>
              <li className="nav">
                <NavLink className=" btn rounded btn-outline-light me-2" to="/user/signup">
                  Sign Up
                </NavLink>
              </li>
            </>
          }
        </div>
      </div>
    </nav>
  )
}