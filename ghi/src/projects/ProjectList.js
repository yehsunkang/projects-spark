import React, { useState, useEffect } from 'react'
import { useGetProjectsQuery, useAddProjectMutation } from '../reduxstore/projectListApi'
import { Container, Row, Col, Table, Form, Spinner, FloatingLabel, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";

export default function ListProjects(props) {
  // ---STATE VARIABLES---

  // complete set of projects from redux store
  const { data, isFetching } = useGetProjectsQuery()

  // redux mutation for adding projects
  const [addProject, result] = useAddProjectMutation()

  // filtered projects after using year filter
  const [yearProjects, setYearProjects] = useState([])

  // filtered projects after using month filter
  const [monthProjects, setMonthProjects] = useState([])

  // filtered projects after using search bar
  const [searchedProjects, setSearchedProjects] = useState([])

  // year selection
  const [year, setYear] = useState(null)

  // month selection
  const [month, setMonth] = useState(null)

  // the search bar
  const [search, setSearch] = useState("")

  // list of years from 2022 to present
  const current_year = new Date().getFullYear()
  const years = []
  for (let i = 2022; i <= current_year; i++) {
    years.push(i)
  }

  // add project form
  const [newId, setNewId] = useState("")

  // ---HELPER FUNCTIONS---

  // Set year state on year select change
  function handleYear(event) {
    setYear(event.target.value)
  }

  // Set month state on month select change
  function handleMonth(event) {
    setMonth(event.target.value)
  }

  // Set search state on search bar change
  function handleSearch(event) {
    setSearch(event.target.value)
  }

  // Set newId state on add project input change
  function handleNewId(event) {
    setNewId(event.target.value)
  }

  // handler function for adding new project
  function handleAddProject(event) {
    event.preventDefault()
    const d = {
      new_id: newId
    }
    addProject(d)
  }
  useEffect(() => {
    if (result.isSuccess) {
      setNewId("")
    }
  }, [result])

  // ---USE EFFECTS---

  // Project Filtering Structure:
  //   - all projects are pulled from redux store
  //   - yearProjects filters out all projects by year
  //   - monthProjects filters out yearProjects by month
  //   - searchedProjects filters out monthProjects by search input
  //   - searchedProjects list is displayed on the table

  // initialize all filtered lists to the full list of projects.
  useEffect(() => {
    if (!isFetching && data) {
      setYearProjects(data.projects)
    }
  }, [isFetching, data])

  // Set year-filtered projects on year state change
  useEffect(() => {
    function yearFilter() {
      if (year && data) {
        const filteredProjects = data.projects.filter(
          project => project.p_start.year === String(year)
        )
        setYearProjects(filteredProjects)
      }
      else if (data) {
        setYearProjects(data.projects)
      }
    }
    if (!isFetching) {
      yearFilter()
    }
  }, [year, data, isFetching])

  // Set month-filtered projects on month state change OR yearProjects change
  useEffect(() => {
    function monthFilter() {
      if (month) {
        const filteredProjects = yearProjects.filter(
          project => project.p_start.month === String(month)
        )
        setMonthProjects(filteredProjects)
      }
      else {
        setMonthProjects(yearProjects)
      }
    }
    if (!isFetching) {
      monthFilter()
    }
  }, [yearProjects, month, isFetching])

  // Set search-filtered projects on search state change OR monthProjects change
  useEffect(() => {
    function searchFilter() {
      // filter for projects that match the search bar in either:
      // project id, group id, project name, group name, or member names.
      const filteredProjects = monthProjects.filter(
        project =>
          project.p_id.toString().includes(search) ||
          (project.g_id ? project.g_id.toString().includes(search) : false) ||
          project.p_name.toLowerCase().includes(search.toLowerCase()) ||
          project.g_name.toLowerCase().includes(search.toLowerCase()) ||
          project.members.join().toLowerCase().includes(search.toLowerCase())
      )
      setSearchedProjects(filteredProjects)
    }
    if (!isFetching) {
      searchFilter()
    }
  }, [monthProjects, search, isFetching])

  // ---LOADING SCREEN---
  if (isFetching) {
    return (
      <Container className="text-white my-5">
        <Row className="text-white d-flex justify-content-center text-center">
          <h3>Loading Gitlab Data...</h3>
          <Spinner className="text-white mt-2" animation="border" />
        </Row>
      </Container>
    )
  }

  // ---PAGE COMPONENT---
  return (
    <div className="px-4 py-6 my-5 text-center main-page-background shadow">
    <Container>
      <Row className='text-white mt-4'>
        <Col>
          <h1>Search for Project</h1>
        </Col>
      </Row>
      <Row className='text-white mt-1'>
        {/* Search Bar */}
        <Col>
          <Form.Control className='text-white mt-2' onChange={handleSearch} type="text" placeholder="Search by IDs, project name, group name, or member names" />
        </Col>
        {/* Year Filter */}
        <Col sm='auto'>
          <FloatingLabel label="Year">
            <Form.Select onChange={handleYear} size='md' style={{ width: '100px' }}>
              <option value="">All</option>
              {years.map(year => {
                return (
                  <option key={year} value={year}>{year}</option>
                )
              })}
            </Form.Select>
          </FloatingLabel>
        </Col>
        {/* Month Filter */}
        <Col sm='auto'>
          <FloatingLabel label="Month">
            <Form.Select onChange={handleMonth} size='md' style={{ width: '100px' }}>
              <option value="">All</option>
              <option value="01">Jan</option>
              <option value="02">Feb</option>
              <option value="03">Mar</option>
              <option value="04">Apr</option>
              <option value="05">May</option>
              <option value="06">Jun</option>
              <option value="07">Jul</option>
              <option value="08">Aug</option>
              <option value="09">Sep</option>
              <option value="10">Oct</option>
              <option value="11">Nov</option>
              <option value="12">Dec</option>
            </Form.Select>
          </FloatingLabel>
        </Col>
      </Row>
      {/* Add a Project - Only show if user is superuser */}
      { props.superuser ?
        <>
          <Row className='text-white mt-1'>
            <Col>
              {/* <h3>Add a Project</h3> */}
            </Col>
          </Row>
          <Form onSubmit={handleAddProject}>
            <Row className='text-white mt-1'>
              <Col xs='auto' >
                <Form.Control type="text" value={newId} onChange={handleNewId} placeholder="Project ID" style={{ width: '150px' }} />
              </Col>
              <Col xs='auto'>
                <Button variant="dark" type="submit">  <FontAwesomeIcon icon={faPlusSquare} /> </Button>
              </Col>
            </Row>
          </Form>
        </>
        :
        false
      }
      {/* Projects Table */}
      <Row className='text-white mt-3'>
        <Col>
          <Table className='text-white'>
            <thead>
              <tr>
                <th></th>
                <th>Project</th>
                <th>Group</th>
                <th>Members</th>
                <th>Started on</th>
              </tr>
            </thead>
            <tbody>
              {searchedProjects.map(project => {
                return (
                  <tr key={project.p_id}>
                    {/* Avatar */}
                    <td style={{ width: '0.1%', whiteSpace: 'nowrap' }}>
                      <Link to={project.p_id}>
                        <img src={project.p_avatar ? project.p_avatar : "../avatar_1.jpg"} alt="" width="50" height="50" />
                      </Link>
                    </td>
                    {/* Project Name & ID */}
                    <td style={{whiteSpace:'nowrap'}}>
                      <Link to={project.p_id}>
                        <span style={{ color: 'white' }}>{project.p_name}</span>
                      </Link>
                      <br />
                      <a href={project.p_gitlab} target="_blank" rel="noreferrer" className='text-white small_impact_font'>id: {project.p_id}</a>
                    </td>
                    {/* Group Name & ID */}
                    <td style={{whiteSpace:'nowrap'}}>
                      <span className={(project.g_name === "N/A" ? "small_impact_font" : "")}>{project.g_name}</span>
                      <br />
                      <span className='text-white small_impact_font'>{project.g_id ? "id: " + project.g_id : ""}</span>
                    </td>
                    {/* Project Members */}
                    <td>
                      {project.members.map(name => {
                        return <span key={name} className='text-white name'>{name}</span>
                      })}
                    </td>
                    {/* Project Start Date */}
                    <td style={{whiteSpace:'nowrap'}}>
                      {project.p_start.year + "-" + project.p_start.month + "-" + project.p_start.day}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
    </div>
  )
}