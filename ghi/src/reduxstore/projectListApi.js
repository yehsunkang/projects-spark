import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import Cookies from 'js-cookie'

export const projectListApi = createApi({
  reducerPath: 'projects',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_PROJECTS_SERVICE
    // prepareHeaders: (headers) => {
    //   const token = Cookies.get("jwt_access_token")
    //   if (token) {
    //     headers.set('authorization', `Bearer ${token}`)
    //   }
    // }
  }),
  tagTypes: ['ProjectList'],
  endpoints: builder => ({
    getProjects: builder.query({
      query: () => ({ url: '/api/projects/' }),
      providesTags: ['ProjectList'],
      keepUnusedDataFor: 3600,
    }),
    addProject: builder.mutation({
      query: data => ({
        url: '/api/projects/',
        body: data,
        method: 'post',
      }),
      invalidatesTags: ['ProjectList'],
    })
  })
})

export const {
  useGetProjectsQuery,
  useAddProjectMutation,
} = projectListApi