import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
// import Cookies from 'js-cookie'

export const userCommentsApi = createApi({
  reducerPath: 'comments',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_ACCOUNTS_SERVICE,
    // prepareHeaders: (headers) => {
    //   const token = Cookies.get("jwt_access_token")
    //   if (token) {
    //     headers.set('authorization', `Bearer ${token}`)
    //   }
    // }
  }),
  tagTypes: ['CommentList'],
  endpoints: builder => ({
    getUserComments: builder.query({
      query: (id) => ({ url: `/api/users/comments/${id}` }),
      providesTags: ['CommentList'],
      keepUnusedDataFor: 3600,
    })
  })
})

export const { useGetUserCommentsQuery } = userCommentsApi