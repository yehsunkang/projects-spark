import React, { useState, useEffect } from "react";
import { Routes, Route } from 'react-router-dom';
import { useToken } from "./users/Auth";
import Nav from './Nav';
import MainPage from './MainPage';
import Signup from './users/Signup';
import Login from "./users/Login";
import Logout from "./users/Logout";
import UserProfile from "./profiles/UserProfile";
import ListProjects from './projects/ProjectList';
import ProjectDetails from "./projects/ProjectDetail";
import jwt_decode from "jwt-decode";


function CustomRouter() {

  const [token, login, logout, signup] = useToken()
  const [data, setData] = useState({user:{perms:[]}})

  useEffect(() => {
    if (!!token) {
      setData(jwt_decode(token))
    }
    else {
      setData({user:{perms:[]}})
    }
  }, [token])

  return (
    <>
      <Nav userId={data.user.id} />
      <Routes>
        <Route path="/">
          <Route index element={<MainPage />} />
          <Route path="user">
            <Route path="signup" element={<Signup signup={signup} />} />
            <Route path="login" element={<Login login={login} />} />
            <Route path="logout" element={<Logout logout={logout} />} />
          </Route>
          <Route path="profiles">
            <Route path="user" element={<UserProfile userId={data.user.id} />} />
          </Route>
          <Route path="projects">
            <Route index element={<ListProjects superuser={data.user.perms.includes("auth.add_permission")} />} />
            <Route path=":id" element={<ProjectDetails userId={data.user.id} />} />
          </Route>
        </Route>
      </Routes>
    </>
  );
}
export default CustomRouter;