import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function Logout(props) {
  const navigate = useNavigate()
  useEffect(() => {
    props.logout()
    navigate('/')
  },[navigate, props])
}