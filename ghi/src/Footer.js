import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquareGitlab } from '@fortawesome/free-brands-svg-icons';


function Footer() {

  return (
    <footer className="py-3 mt-2 fixed" style={{ backgroundColor: 'black' }}>
      <h2 className="nav nav-item  justify-content-center"> About Us </h2>
      <ul className="nav justify-content-center">
        <li className="nav">
          <a className="nav-link" href="https://gitlab.com/brendanbird12">Brendan</a>
        </li>
        <li className="nav">
          <a className="nav-link" href="https://www.linkedin.com/in/yehsun-kang-aa7040164/">Yehsun</a>
        </li>
        <li className="nav">
          <a className="nav-link" href="https://www.linkedin.com/in/liying-liao/">Liying</a>
        </li>
        <li className="nav">
          <a className="nav-link" href="https://www.linkedin.com/in/teresamtan/">Teresa</a>
        </li>
        <li className="nav">
          <a className="nav-link" href="https://gitlab.com/chickenjon">Jonathan</a>
        </li>
      </ul>
      <div className="footer-copyright text-center py-3">
        ©2022 project spark
        <a className="footer-link-color p-2" href="https://gitlab.com/salmons1/salmon-project">
          <FontAwesomeIcon icon={faSquareGitlab} />
        </a>
  
      </div>
    </footer>
  );
}



export default Footer;


