import React from 'react'
import { Container, Row, Col, Table, Spinner } from 'react-bootstrap'
import { Carousel } from "react-bootstrap"
import { useGetProjectsQuery } from './reduxstore/projectListApi.js'
import './mainpage.css';


export default function MainPage() {
  const { data, isFetching } = useGetProjectsQuery()

  if (isFetching) {
    return (
      <Container className="my-5 text-white">
        <Row className="d-flex justify-content-center text-center">
          <h3>Loading Gitlab Data...</h3>
          <Spinner className="mt-2" animation="border" />
        </Row>
      </Container>
    )
  }

  return (
    <>
     <div className="px-4 py-10 my-10 text-center main-page-background shadow">
      <Container>
        <h1 className="my-4 display-7 fw-bold text-center ">Hack Reactor's Student-Made Web Applications</h1>
        <Carousel className="mt-5">
          {data ?
            data.projects.map(project => {
              return (
                <Carousel.Item key={project.p_id} className='carousel-inner'>
                  <Row>
                    <Col className='text-end'>
                      <img src={project.p_avatar ? project.p_avatar : "../avatar_1.jpg"} alt="" width="250px" height="250px" />
                    </Col>
                    <Col className='ms-5 mt-3'>
                      <Table borderless="true" className='text-white' size='sm'>
                        <tbody>
                          {project.members.map(name => {
                            return (
                              <tr key={name}>
                                <td style={{ paddingLeft: '8px', fontSize: '15px' }}>
                                  {name}
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Carousel.Item>
              )
            })
            :
            "Hello"
          }
        </Carousel>
      </Container>
      </div>
    </>
  );
}
