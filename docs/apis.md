# APIS

## Endpoints for projects:

* **Method**: `GET`
* **Path**: /api/projects/

Output:
```json
{
	"projects": [
		{
			"g_id": int,
			"g_name": string,
			"g_avatar": int,
			"members": [
				string,
				...
			],
			"p_id": string,
			"p_name": string,
			"p_avatar": string,
			"p_start": {
				"year": string,
				"month": string,
				"day": string
			},
			"p_gitlab": string
		},
    {
      ...
    }
  ]
}
```
Returns a list of projects under the key "projects", each project has detailed information.

* **Method**: `POST`
* **Path**: /api/projects/

Input:
```json
{
  "project_id": string
}
```
Output:
```json
{
	"project": {
			"g_id": int,
			"g_name": string,
			"g_avatar": int,
            "g_gitlab": string,
			"p_id": string,
			"p_name": string,
			"p_avatar": string,
			"p_start": string,
			"p_gitlab": string,
            "total_commits_num": int,
            "counts_by_author": [
              {
                "name": "Jonathan Yoo",
                "count": 156
              },
                  ...
              {
                "name": "Teresa",
                "count": 72
              }
            ],
            "members": [
            {
              "name": string,
              "url": string,
              "avatar_url": string
            },
                  ...
            ],
            "live_url": string,
            "readme": string,
            "wireframes": [
              string,
              ...
              string
            ],
            "languages": [
              {
                "language": string,
                "percentage": float
              },
                    ...
            ]
	}
}
```
Returns a project object based on project_id under the key "project", each project has detailed information.

* **Method**: `GET`
* **Path**: /api/projects/ids/

Output:
```json
{
	"ids": [
    string,
    string,
    ...
    string
  ]
}
```
Returns a list of project ids under the key "ids", each id is a project id in GitLab.

* **Method**: `GET`
* **Path**: /api/projects/{project_id}

Output:
```json
{
	"project": {
			"g_id": int,
			"g_name": string,
			"g_avatar": int,
            "g_gitlab": string,
			"p_id": string,
			"p_name": string,
			"p_avatar": string,
			"p_start": string,
			"p_gitlab": string,
            "total_commits_num": int,
            "counts_by_author": [
              {
                "name": "Jonathan Yoo",
                "count": 156
              },
                  ...
              {
                "name": "Teresa",
                "count": 72
              }
            ],
            "members": [
            {
              "name": string,
              "url": string,
              "avatar_url": string
            },
                  ...
            ],
            "live_url": string,
            "readme": string,
            "wireframes": [
              string,
              ...
              string
            ],
            "languages": [
              {
                "language": string,
                "percentage": float
              },
                    ...
            ]
	}
}
```
Returns a detailed project information under the key "project", each project has detailed information we need to display the detail page.

* **Method**: `GET`
* **Path**: /api/projects/comments/

Output:
```json
{
	"comments": [
		{
			"id": int,
			"text": string,
			"user": foreign key,
			"timestamp": date,
			"project": foreign key
		},
    {
      ...
    }
  ]
}
```
Returns a list of comments under the key "comments", each comment has detailed information.

* **Method**: `POST`
* **Path**: /api/projects/comments/

Input:
```json
{
  "text": string,
}
```

Output:
```json
{
	"comment": 
		{
			"id": int,
			"text": string,
			"user": foreign key,
			"timestamp": date,
			"project": foreign key
		}
}
```

* **Method**: `GET`
* **Path**: /api/projects/{project_id}/comments/

Output:
```json
{
	"comments": [
		{
			"id": int,
			"text": string,
			"user": foreign key,
			"timestamp": date,
			"project": foreign key
		},
    {
      ...
    }
  ]
}
```
Returns a list of comments under the key "comments" for a specific project, each comment has detailed information.

* **Method**: `GET`
* **Path**: /api/users/{user_id}

Output:
```json
{
	"user": {
      "username": string,
      "avatar_id": int
	}
}
```
Returns user under the key "user" for a specific project, each user has detailed information. This endpoint is calling the UserVO model.

## Endpoints for accounts:
---
* **Method**: `GET`
* **Path**: /api/users

Output:
```json
{
	"users": [
		{
			"id": int,
			"username": string,
			"bio": string,
			"avatar_id": int
		},
		...
		{
			"id": int,
			"username": string,
			"bio": string,
			"avatar_id": int
		}
	]
}
```
Returns all users under the key "users", each user has detailed information. This endpoint is calling the User model directly

* **Method**: `POST`
* **Path**: /api/users

Input:
```json
{
			"username": string,
			"password": hash,
			"bio": string,
			"avatar_id": int
}
```
Output:
```json
{
	"user": {
		"id": int,
		"email": string,
		"username": string,
		"bio": string,
		"avatar_id": int
	}
}
```
Creates a new user by passing the required fields, returns the new user under key user.

* **Method**: `GET`
* **Path**: /api/users/<int:pk>
Output:
```json
{
	"user": {
		"id": int,
		"email": string,
		"username": string,
		"bio": string,
		"avatar_id": int
	}
}
```
Creates the user detail based on the pk in database. 

* **Method**: `GET`
* **Path**: /api/tokens/mine/
Output:
```json
{
	"token": hash
}
```
Returns the jwt_access_token from request.cookies if there's any, otherwise, returns None.

