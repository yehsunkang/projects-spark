# Data models

<!-- ## In accounts service: -->

## User

| bio | avatar_id | 
|-|-|
| string | int |


<!-- ## In projects service: -->
## UserVO

| username | bio | avatar_id | 
|-|-|-|
| string | string | int |

## ProjectId

| project_id |
|-|
| string |

<!-- | user_id | team_name | preview_img | link | description | tags | tech | comments | ratings |
|-|-|-|-|-|-|-|-|-|
| foreign key | foreign key | img link | url | text | foreign key | foreign key | foreign key | foreign key | -->

## Comment

| text | timestamp | user | project |
|-|-|-|-|
| string | datetime | foreign key | foreign key |

## CommentVO

| text | timestamp | user | project |
|-|-|-|-|
| string | datetime | foreign key | foreign key |

