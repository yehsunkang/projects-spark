# Integrations

The application requires integrating data from each project's gitlab.

* [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/api-usage.html)
   - Provides a access token for full list of available public resources.
   - The gitlab.Gitlab class provides managers to access the GitLab resources. Each manager provides a set of methods to act on the resources. The available methods depend on the resource type.
* [GitLab Docs](https://docs.gitlab.com/ee/api/projects.html): 
    - Get a list of the public project can be accessed without any authentication.
    - Retrieve data from the each project for the attribute: projectId, project name, group name, group id, team members, language usage, commits distributions,started date. 
