from django.urls import path

from .views import (
    api_user_token,
    get_comments_by_user,
    users,
    get_user,
)

urlpatterns = [
    path("users/", users, name="user_signup"),
    path("tokens/mine/", api_user_token, name="user_token"),
    path("users/<int:pk>/", get_user, name="user_detail"),
    path(
        "users/comments/<int:pk>",
        get_comments_by_user,
        name="user_comments",
    ),
]
