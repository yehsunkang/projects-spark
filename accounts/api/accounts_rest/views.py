# import os
import gitlab
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import User, CommentVO
from common.json import ModelEncoder
from django.db import IntegrityError

gl = gitlab.Gitlab(private_token='glpat-neskdn7NxgfTrGrSTQ_s')

# import djwto.authentication as auth
# @auth.jwt_login_required


class UserEncoder(ModelEncoder):
    model = User
    properties = [
        "id",
        "username",
        "bio",
        "avatar_id",
    ]


class CommentVOEncoder(ModelEncoder):
    model = CommentVO
    properties = [
        "id",
        "user_id",
        "project_id",
        "text",
        "timestamp",
    ]


@require_http_methods(["GET"])
def api_user_token(request):
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            return JsonResponse({"token": token})
    response = JsonResponse({"token": None})
    return response


@require_http_methods(["GET", "POST"])
def users(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            user = User.objects.create_user(
                username=content["username"],
                password=content["password"],
                # email=content["email"],
            )
            return JsonResponse(
                {"username": user},
                safe=False,
                encoder=UserEncoder,
            )
        except IntegrityError:
            response = JsonResponse({
                "detail": "Please enter a different username"
            })
            response.status_code = 409
            return response
    else:
        users = User.objects.all()
        return JsonResponse({"users": users}, encoder=UserEncoder, safe=False)


@require_http_methods(["GET"])
def get_user(request, pk):
    user = User.objects.get(id=pk)
    return JsonResponse({"user": user}, encoder=UserEncoder, safe=False)


@require_http_methods(["GET"])
def get_users(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse({"users": users}, encoder=UserEncoder, safe=False)


@require_http_methods(["POST"])
def create_user(request):
    content = json.loads(request.body)
    user = User.objects.create(**content)
    return JsonResponse(user, encoder=UserEncoder, safe=False)


@require_http_methods(["PUT"])
def update_user(request, pk):
    content = json.loads(request.body)
    user = User.objects.update_or_create(
        id=pk,
        defaults=content,
    )
    return JsonResponse(user, encoder=UserEncoder, safe=False)


@require_http_methods(["DELETE"])
def delete_user(request, pk):
    if request.method == "DELETE":
        count, _ = User.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def get_comments_by_user(request, pk):
    comments = CommentVO.objects.filter(user_id=pk)
    print("COMMENTSSSSSSSSSSSSSSSSSSS:", comments)
    comment_list = []
    for comment in comments:
        project = gl.projects.get(id=comment["project_id"])
        p_name = project.name
        p_avatar = project.avatar_url
        comment_list.append(
            {
                **comment,
                "p_name": p_name,
                "p_avatar": p_avatar,
            }
        )
    return JsonResponse({"comments": comment_list})
