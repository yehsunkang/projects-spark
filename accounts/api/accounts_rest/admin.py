from django.contrib import admin
from accounts_rest.models import User, CommentVO


class UserAdmin(admin.ModelAdmin):
    pass


class CommentVOAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(CommentVO, CommentVOAdmin)
