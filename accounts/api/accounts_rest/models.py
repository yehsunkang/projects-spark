from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class CommentVO(models.Model):
    user_id = models.IntegerField()
    project_id = models.IntegerField()
    text = models.TextField()
    timestamp = models.DateTimeField()

    class Meta:
        ordering = ("-timestamp",)

    def __str__(self):
        return f"id:{self.id} - proj:{self.project_id} - author:{self.user_id}"


class User(AbstractUser):
    bio = models.TextField(max_length=500, null=True, blank=True)
    avatar_id = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        default=1,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(6),
        ],
    )
