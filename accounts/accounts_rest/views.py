
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# from django.contrib.auth.models import User
from .models import User
from common.json import ModelEncoder
from django.db import IntegrityError

import djwto.authentication as auth


# @auth.jwt_login_required

class UserEncoder(ModelEncoder):
    model = User
    properties = [
        # "id",
        # "email",
        "username",
        # "bio",
        # "avatar_id",
    ]


@require_http_methods(["GET", "POST"])
def users(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            user = User.objects.create_user(
                username=content["username"],
                password=content["password"],
                # email=content["email"],
            )
            return JsonResponse(
                {"username": user},
                safe=False,
                encoder=UserEncoder,
            )
        except IntegrityError:
            response = JsonResponse(
                {"detail": "Please enter a different username"}
            )
            response.status_code = 409
            return response
    else:
        users = User.objects.all()
        return JsonResponse({"users": users}, encoder=UserEncoder, safe=False)


@require_http_methods(["GET"])
def user_token(request):
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            return JsonResponse({"token": token})
    response = JsonResponse({"token": None})
    return response


@require_http_methods(["POST"])
def create_user(request):
    content = json.loads(request.body)
    user = User.objects.create(**content)
    return JsonResponse(
        user,
        encoder=UserEncoder, 
        safe=False
    )


@require_http_methods(["DELETE"])
def delete_user(request, pk):
    if request.method == "DELETE":
        count, _ = User.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def get_users(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse(
            {"users": users},
            encoder=UserEncoder,
            safe=False
        )

        # try:
        #     content = json.loads(request.body)
        # except json.JSONDecodeError:
        #     return 400, {"message": "Bad JSON"}, None

        # required_properties = [
        #     "username",
        #     "avatar_id",
        #     "password",
        #     "bio",
        # ]
        # missing_properties = []
        # for required_property in required_properties:
        #     if (
        #         required_property not in content
        #         or len(content[required_property]) == 0
        #     ):
        #         missing_properties.append(required_property)
        # if missing_properties:
        #     response_content = {
        #         "message": "missing properties",
        #         "properties": missing_properties,
        #     }
        #     print ("RESPONSE_CONTENT: ", response_content)
        #     return render(400, response_content, None)

        # try:
        #     account = User.objects.create_user(
        #         username=content["username"],
        #         avatar_id=content["avatar_id"],
        #         password=content["password"],
        #         bio=content["bio"],
        #     )
        #     return 200, account, account
        # except IntegrityError as e:
        #     return 409, {"message": str(e)}, None
        # except ValueError as e:
        #     return 400, {"message": str(e)}, None
    







# @require_http_methods(["POST"])
# def create_user(request):
#     try:
#         content = json.loads(request.body)
#     except json.JSONDecodeError:
#         return 400, {"message": "Bad JSON"}, None

#     required_properties = [
#         "username",
#         "password",
#         "bio",
#         # "avatar_id",
#     ]
#     missing_properties = []
#     for required_property in required_properties:
#         if (
#             required_property not in content
#             or len(content[required_property]) == 0
#         ):
#             missing_properties.append(required_property)
#     if missing_properties:
#         response_content = {
#             "message": "missing properties",
#             "properties": missing_properties,
#         }
#         return 400, response_content, None

#     try:
#         account = User.objects.create_user(
#             username=content["username"],
#             password=content["password"],
#             bio=content["bio"],
#             # avatar_id=content["avatar_id"],
#         )
#         return redirect("http://projects:8000/api/projects/")
#     except IntegrityError as e:
#         return 409, {"message": str(e)}, None
#     except ValueError as e:
#         return 400, {"message": str(e)}, None


# @require_http_methods(["POST"])
# def update_user(json_content, request):
#     try:
#         content = json.loads(json_content)
#     except json.JSONDecodeError:
#         return 400, {"message": "Bad JSON"}, None
#     try:
#         account = User.objects.get(id=request.user.id)
#         for key, value in content.items():
#             account[key] = value
#         account.save()

#         return 200, account, account
#     except IntegrityError as e:
#         return 409, {"message": str(e)}, None
#     except ValueError as e:
#         return 400, {"message": str(e)}, None



# @require_http_methods(["POST"])
# def update_password(json_content, request):
#     try:
#         content = json.loads(json_content)
#     except json.JSONDecodeError:
#         return 400, {"message": "Bad JSON"}, None
#     try:
#         account = authenticate(
#             username=request.user.username,
#             password=content["old_password"],
#         )
#         account.set_password(content["new_password"])
#         account.save()

#         return 200, account, account
#     except IntegrityError as e:
#         return 409, {"message": str(e)}, None
#     except ValueError as e:
#         return 400, {"message": str(e)}, None
