import django
import os
import json
import requests
import sys
import time

PROJECTS_API = os.environ["PROJECTS_API"]

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "accounts_project.settings")
django.setup()

from accounts_rest.models import CommentVO


def get_comments():
    response = requests.get(f"{PROJECTS_API}/api/projects/comments/")
    content = json.loads(response.content)
    for comment in content:
        CommentVO.objects.update_or_create(
            id=comment["id"],
            defaults={
                "user_id": comment["user"]["id"],
                "project_id": comment["project"]["project_id"],
                "text": comment["text"],
                "timestamp": comment["timestamp"],
            },
        )


def poll():
    while True:
        try:
            get_comments()
            print("Accounts service finished polling for CommentVO.")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()
