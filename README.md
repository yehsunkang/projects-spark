# ProjectSpark

A central hub to view, collaborate, and comment on all cohort's projects from the 19-week Hack Reactor Software Engineering Immersive program. Students of the May cohort and all future cohorts that could benefit from having a website dedicated to displaying sample projects, as well as all Hack Reactor staff and alumni. 
Feel free to sign up and poke around on our [site](https://salmons1.gitlab.io/salmon-project/)

# Technologies Used
* Python
* JavaScript
* PostgreSQL
* Django
* Docker
* RESTful API's
* SimpleJWT auth
* External API Integrations
[python-gitlab](https://python-gitlab.readthedocs.io/en/stable/api-usage.html)
[GitLab Docs](https://docs.gitlab.com/ee/api/projects.html): 



# GHI Design


## Home
The main page carousel projects with logos and name of the team members.

![home page](docs/wireframes/main.png)


## Project List
This page lists all the projects. The search bar allows to find projects by id, name, group name, and member name. 
Also we can add projects by project id. Clicking logos lead to the detail page. 

![ist page](docs/wireframes/list.png)


## Project Detail
The detail page shows group and project information. Bar chart of languages used. Pie chart breaking down the number of commits per member. Also can write the comment. When click "Open in new tab" lead to the deployed page.

![detail page](docs/wireframes/detail.png)


## Profile
Changing avatar and bio. 

![profile page](docs/wireframes/profile.png)


<!-- 
## <u>***Functionality***</u>

- Anyone can view all final projects from the 19-week Hack Reactor Software Engineering Immersive program
- Only users who signup and login may leave a comment 
- Search function to find projects based on project name, owners, month and year, etc.
- Project detail page features 
    * preview of their website if deployed
    * README file
    * all GitLab group and project information 
    * miniaturized view of wireframe files
    * bar chart of languages used
    * pie chart breaking down the number of commits per member

 -->
