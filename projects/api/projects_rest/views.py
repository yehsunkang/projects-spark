# import os
import gitlab
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# import djwto.authentication as auth
from .models import ProjectId, Comment, UserVO
from common.json import ModelEncoder

from collections import defaultdict
from difflib import SequenceMatcher

gl = gitlab.Gitlab(private_token='glpat-neskdn7NxgfTrGrSTQ_s')


# Encoders --------------------------------------------------


class UserVOEncoder(ModelEncoder):
    model = UserVO
    properties = [
        "id",
        "username",
        "avatar_id",
    ]


class ProjectIdEncoder(ModelEncoder):
    model = ProjectId
    properties = [
        "project_id",
    ]


class CommentEncoder(ModelEncoder):
    model = Comment
    properties = [
        "id",
        "user",
        "project",
        "text",
        "timestamp",
    ]
    encoders = {
        "user": UserVOEncoder(),
        "project": ProjectIdEncoder(),
    }


# Helper Functions ------------------------------------------


def get_project_from_id(pk):
    project = gl.projects.get(id=pk)
    return project


def get_group_from_project(project):
    p_list = project.groups.list()
    if p_list:
        group = p_list[0]
        return group
    return None


def get_members_from_project(project):
    instructor_ids = [10232299, 12206692]
    # [Daniel Billotte, Andrew Singley]
    i_list = filter(lambda x: x.id not in instructor_ids, project.users.list())
    return i_list


def similarity(a, b):
    return SequenceMatcher(None, a, b).ratio()


# View Functions --------------------------------------------


@require_http_methods(["GET"])
def get_userVO(request, pk):
    user = UserVO.objects.get(id=pk)
    result = {
        "username": user.username,
        "avatar_id": user.avatar_id,
    }
    return JsonResponse({"user": result})


@require_http_methods(["GET"])
def get_list_of_project_ids(request):
    project_ids = list(ProjectId.objects.values_list("project_id", flat=True))
    return JsonResponse(
        {"ids": project_ids},
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def get_projects(request):
    # Get all projects
    if request.method == "GET":
        # Gitlab Variables
        project_ids = ProjectId.objects.values_list("project_id", flat=True)
        projects = []
        for id in project_ids:
            project = get_project_from_id(id)
            group = get_group_from_project(project)
            gl_members = get_members_from_project(project)

            # Group
            if group:
                g_id = group.id
                g_name = group.name
                g_avatar = group.avatar_url
            else:
                g_id = None
                g_name = "N/A"
                g_avatar = None

            # Members
            members = []
            for member in gl_members:
                members.append(member.name)
            members.sort(key=str.casefold)

            # Project Start Date
            created_at = project.created_at
            created_at = created_at[0:10]
            date = created_at.split("-")
            p_start = {
                "year": date[0],
                "month": date[1],
                "day": date[2],
            }

            # Project Gitlab Link
            p_gitlab = project.web_url

            # Add to list
            projects.append(
                {
                    "g_id": g_id,
                    "g_name": g_name,
                    "g_avatar": g_avatar,
                    "members": members,
                    "p_id": id,
                    "p_name": project.name,
                    "p_avatar": project.avatar_url,
                    "p_start": p_start,
                    "p_gitlab": p_gitlab,
                }
            )

        # Return Response
        return JsonResponse({"projects": projects})
    # Add a new project
    else:
        content = json.loads(request.body)
        new_id = content["new_id"]

        try:
            get_project_from_id(new_id)

            new_project = {"project_id": new_id}
            project = ProjectId.objects.create(**new_project)
            return JsonResponse(
                project,
                encoder=ProjectIdEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse({"error": "Project not found on Gitlab."})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def project_detail(request, pk):
    # Gitlab Variables
    project = get_project_from_id(pk)
    group = get_group_from_project(project)

    # Commits
    commits = project.commits.list(get_all=True, all=True, ref_name="main")
    total_commits_num = len(commits)
    counts_by_author_pre = defaultdict(lambda: 0)

    for commit in commits:
        author_name = commit.author_name
        if author_name == "Daniel Billotte":
            total_commits_num -= 1
        else:
            for name in counts_by_author_pre:
                if (
                    similarity(author_name, name) >= 0.6
                    or author_name in name
                    or name in author_name
                ):
                    author_name = name
                    break
            counts_by_author_pre[author_name] += 1

    counts_by_author = []
    for name, count in counts_by_author_pre.items():
        counts_by_author.append(
            {
                "name": name,
                "count": count,
            }
        )
    counts_by_author.sort(key=lambda x: x["count"], reverse=True)

    # Group
    if group:
        g_id = group.id
        g_name = group.name
        g_avatar = group.avatar_url
        g_gitlab = group.web_url
    else:
        g_id = None
        g_name = "N/A"
        g_avatar = None
        g_gitlab = None

    # Members
    gl_members = get_members_from_project(project)
    members = []
    for member in gl_members:
        members.append(
            {
                "name": member.name,
                "url": member.web_url,
                "avatar_url": member.avatar_url,
            }
        )
    members.sort(key=lambda x: x["name"])

    # Project
    p_start = project.created_at
    p_start = p_start[0:10]
    p_gitlab = project.web_url

    # Live URL
    live_url = None
    try:
        gitlab_ci = project.files.raw(file_path=".gitlab-ci.yml", ref="main")
        file = gitlab_ci.decode()
        lines = file.splitlines()
        for row in lines:
            if "PUBLIC_URL:" in row:
                idx = row.index("https://")
                live_url = row[idx:]
                if " " in live_url:
                    idx = live_url.index(" ")
                    live_url = live_url[:idx]
                break
    except Exception:
        pass

    # Readme File
    readme = "README.md not found on Gitlab"
    try:
        readme_encoded = project.files.raw(file_path="README.md", ref="main")
        readme = readme_encoded.decode()
        readme = readme.replace(
            "(docs/",
            "(" + p_gitlab + "/-/blob/main/docs/",
        )
    except Exception:
        pass

    # Wireframes
    images = project.repository_tree(path="docs/wireframes")
    wireframes = []
    for image in images:
        wireframes.append(p_gitlab + "/-/raw/main/" + image["path"])

    # Programming Languages
    language_data = project.languages()
    languages = []
    for language, percentage in language_data.items():
        languages.append({"language": language, "percentage": percentage})

    # Send Response
    result = {
        "g_id": g_id,
        "g_name": g_name,
        "g_avatar": g_avatar,
        "g_gitlab": g_gitlab,
        "p_id": pk,
        "p_name": project.name,
        "p_avatar": project.avatar_url,
        "p_start": p_start,
        "p_gitlab": p_gitlab,
        "total_commits_num": total_commits_num,
        "counts_by_author": counts_by_author,
        "members": members,
        "live_url": live_url,
        "readme": readme,
        "wireframes": wireframes,
        "languages": languages,
    }

    return JsonResponse({"project": result})


@require_http_methods(["GET"])
def get_project_comments(request, pk):
    project = ProjectId.objects.get(project_id=pk)
    comments = Comment.objects.filter(project=project)
    return JsonResponse(
        comments,
        encoder=CommentEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def get_all_comments(request):
    comments = Comment.objects.all()
    return JsonResponse(
        comments,
        encoder=CommentEncoder,
        safe=False,
    )


# @auth.jwt_login_required
@require_http_methods(["POST"])
def create_comment(request):
    content = json.loads(request.body)
    user = UserVO.objects.get(id=content["user_id"])
    project = ProjectId.objects.get(project_id=content["project_id"])
    text = content["text"]
    new_comment = {
        "user": user,
        "project": project,
        "text": text,
    }

    comment = Comment.objects.create(**new_comment)
    return JsonResponse(
        comment,
        encoder=CommentEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def projects_main(request):
    project_ids = (
        ProjectId
        .objects.order_by("?")
        .values_list("project_id", flat=True)
        .first(5)
    )
    projects = []
    for id in project_ids:
        project = get_project_from_id(id)
        gl_members = get_members_from_project(project)

        # Members
        members = []
        for member in gl_members:
            members.append(member.name)
        members.sort(key=str.casefold)

        # Add to list
        projects.append(
            {
                "members": members,
                "p_id": id,
                "p_name": project.name,
                "p_avatar": project.avatar_url,
            }
        )

    # Return Response
    return JsonResponse({"projects": projects})
