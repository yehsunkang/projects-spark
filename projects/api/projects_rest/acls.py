import json
import requests
import os

PEEKALINK_API_KEY = os.environ["PEEKALINK_API_KEY"]


def get_preview_img(url):
    headers = {"X-API-Key": PEEKALINK_API_KEY}
    response = requests.post(
        "https://api.peekalink.io/",
        headers=headers,
        data={"link": url},
    )
    content = json.loads(response.content)
    print(content)
    # image = content["image"]["url"]
    return "someting for now"
