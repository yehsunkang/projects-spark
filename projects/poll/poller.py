import django
import os
import json
import requests
import sys
import time

ACCOUNTS_API = os.environ["ACCOUNTS_API"]

sys.path.append("")
os.environ["DJANGO_SETTINGS_MODULE"] = "projects_project.settings"
django.setup()

from projects_rest.models import UserVO


def get_users():
    response = requests.get(f"{ACCOUNTS_API}/api/users/")
    content = json.loads(response.content)
    for user in content["users"]:
        UserVO.objects.update_or_create(
            id=user["id"],
            defaults={
                "username": user["username"],
                "bio": user["bio"],
                "avatar_id": user["avatar_id"],
            },
        )
        print(user)


def poll():
    while True:
        try:
            get_users()
            print("Projects service finished polling for UserVO.")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()
